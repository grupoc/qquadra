from django.urls import path

from . import views

app_name = 'reservas'
urlpatterns = [        
    path('', views.reservas, name='index'),
    path('<int:modalidade_id>/', views.detail_modalidade, name='detail'),
    path('espaco/<int:espaco_id>/', views.detail_espaco, name='detail-espaco'),
    path('espaco/dia/<int:pk>', views.detail_dia, name='detail-dia'),
    path('espaco/confirmar/<int:pk>', views.create_reserva, name='create-reserva'),
]