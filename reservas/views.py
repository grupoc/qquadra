from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.contrib.auth.decorators import login_required
from accounts.models import Usuario,Clube, EspacoEsportivo, Dia,Horario,Modalidade
from datetime import timedelta
import datetime as dt
from django.utils import timezone



@login_required(redirect_field_name='login')
def reservas(request):
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube
        context = {'clube': clube,
                    }
    return render(request, 'reservas/reservas.html', context)



def detail_modalidade(request, modalidade_id):
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube
        modalidade = get_object_or_404(Modalidade,pk=modalidade_id)
        espaco = EspacoEsportivo.objects.filter(modalidade=modalidade_id,clube=clube.id)
        context = {'clube': clube,
                    'espaco':espaco,
                    'modalidade':modalidade}
    return render(request, 'reservas/modalidades.html', context)



def detail_espaco(request, espaco_id):
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube
        espaco = get_object_or_404(EspacoEsportivo,id=espaco_id)
        dias = Dia.objects.filter(espaco=espaco.id)
        context = {'clube': clube,
                    'espaco':espaco,
                    'dias': dias}
    return render(request, 'reservas/espaco.html', context)



def detail_dia(request, pk):
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube
        dia = get_object_or_404(Dia,pk=pk)
        espaco = dia.espaco
        horarios = Horario.objects.filter(dia=dia.id)
        context = {'clube': clube,
                    'espaco':espaco,
                    'dia': dia,
                    'horarios': horarios,
                    }
    return render(request, 'reservas/dia.html', context)



def create_reserva(request,pk):
    usuario = request.user
    hora = Horario.objects.filter(usuario=usuario.id)
    quantidade = len(hora)
    if (str(usuario.groups.all()[0]) == "publico" and quantidade <= 1) or (str(usuario.groups.all()[0]) == "atletica" and quantidade <= 29) or (str(usuario.groups.all()[0]) == "admin"):
        print("VEIO PRA CA")
        horario = get_object_or_404(Horario, pk=pk)
        if request.method == "POST":
            horario.livre = True
            horario.usuario = request.user
            horario.save()
            return HttpResponseRedirect(reverse('reservas:detail-dia', args=(horario.dia.id, )))

        context = {'horario': horario}
        return render(request, 'reservas/create-reserva.html', context)
    else:
        return HttpResponse("Você já fez reservas demais")



def create_dias(dias,clube):
        
    espacos = EspacoEsportivo.objects.filter(clube=clube.id)
    for espaco in espacos:
        today = timezone.now().date() 
        for i in range(dias):
            dia = Dia.objects.create(espaco=espaco,dia=today)  
            start = clube.horario_inicial  
            while start != clube.horario_final:
                
                Horario.objects.create(dia=dia,horario=start)
                start = (dt.datetime.combine(dt.date(1,1,1), start) + dt.timedelta(hours=1)).time()
                
            today = today + dt.timedelta(days=1)

    