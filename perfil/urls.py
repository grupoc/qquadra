from django.urls import path

from . import views

app_name = 'perfil'
urlpatterns = [        
    path('', views.perfil, name='index'),
    path('update_usuario/<int:pk>/', views.UserUpdateView.as_view(), name='update'),
]