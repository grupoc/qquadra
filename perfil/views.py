from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.contrib.auth.decorators import login_required
from .forms import UserChangeForm
from accounts.models import Clube, Atletica, Usuario



@login_required(redirect_field_name='login')
def perfil(request):  
    usuario = request.user
    grupo = usuario.groups
    reservas_exames = usuario.horario_exames_set.all()
    reservas = usuario.horario_set.all()
    
    context = {'usuario': usuario,
                'grupo': grupo,   
                'reservas': reservas,
                'reservas_exames': reservas_exames,   }   
    return render(request, 'perfil/perfil.html', context)



class UserUpdateView(generic.UpdateView):
    context_object_name = 'user'
    form_class = UserChangeForm
    model = Usuario
    template_class = UserChangeForm
    template_name = "perfil/update.html"

    def get_success_url(self):
        return reverse('perfil:index')