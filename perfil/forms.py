from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from django import forms
from django.contrib.auth.models import User, Group
from accounts.models import Clube, Atletica, Usuario


class UserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = Usuario
        fields = ('is_active','username', 'email','nome','clube','atletica','foto')