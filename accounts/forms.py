from django.forms import ModelForm
from django.contrib.auth.forms import UserCreationForm,UserChangeForm
from django import forms
from django.contrib.auth.models import User, Group
from .models import Clube, Atletica, Usuario


class UserGroupForm(UserCreationForm):
    class Meta(UserCreationForm.Meta):
        model = Usuario
    nome = forms.CharField(label = "Nome")
    telefone = forms.CharField(label = "Telefone")
    foto = forms.URLField(label = "URL da Foto de Perfil",required=False)
    email = forms.CharField(label = "Endereço de E-mail")
    type = forms.ChoiceField(choices=(("publico","Público"),("atletica","Atlética")), required=True, label="Tipo de Conta")
    clube = forms.ModelChoiceField(queryset=Clube.objects.all(), required=True)
    atletica = forms.ModelChoiceField(queryset=Atletica.objects.all(), required=False, label = "Atlética")

    def save(self, commit=True):
        user = super(UserGroupForm, self).save(commit=False)
        user.nome = self.cleaned_data["nome"]
        user.telefone = self.cleaned_data["telefone"]
        user.foto = self.cleaned_data["foto"]
        user.email = self.cleaned_data["email"]
        user.type = self.cleaned_data["type"]
        user.clube = self.cleaned_data["clube"]
        user.atletica = self.cleaned_data["atletica"]
        

        if commit:
            user.save()
        return user
    
class UserChangeForm(UserChangeForm):
    class Meta(UserChangeForm.Meta):
        model = Usuario
        fields = ('is_active','username', 'email','nome','groups','clube','atletica')