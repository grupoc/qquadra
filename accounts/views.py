from django.shortcuts import render, get_object_or_404
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib.auth.models import Group
from .forms import UserGroupForm,UserChangeForm
from .models import Usuario,Horario,Horario_Exames
from django.contrib.auth.decorators import login_required
from django.views import generic
from .decorators import allowed_users



def signup(request):
    if request.method == 'POST':
        form = UserGroupForm(request.POST)
        if form.is_valid():
            user = form.save()
            user_group = Group.objects.get(name=user.type)
            user.groups.add(user_group)  
            return HttpResponseRedirect(reverse('login'))
    else:
        form = UserGroupForm()

    context = {'form': form}
    return render(request, 'accounts/signup.html', context)



def logoutUser(request):
	logout(request)
	return redirect('login')



@login_required
@allowed_users(allowed_roles=['admin'])
def list_users(request):
    usuario = request.user
    clube=usuario.clube
    users_list = Usuario.objects.filter(clube=clube.id)
    context = {'users_list': users_list}
    
    return render(request, 'accounts/list.html', context)



def menu_reservas_admin(request):
    return render(request, 'accounts/menu-reservas-admin.html')



def reservas_espaco_admin(request):
    clube = request.user.clube
    horarios = Horario.objects.filter(livre=True)
    context = {'clube': clube,
                'horarios': horarios,}

    return render(request, 'accounts/list-reservas-admin.html',context)



def delete_reserva(request,pk):
    horario = get_object_or_404(Horario, pk=pk)
    if request.method == "POST":
        horario.livre = False
        horario.usuario = None
        horario.save()
        return HttpResponseRedirect(reverse('accounts:menu-reservas-admin'))

    context = {'horario': horario}
    return render(request, 'accounts/delete-reserva.html', context)



def reservas_exames_admin(request):
    clube = request.user.clube
    horarios = Horario_Exames.objects.filter(livre=True)
    context = {'clube': clube,
                'horarios': horarios,}
    
    return render(request, 'accounts/list-reservas-exames-admin.html',context)



def delete_reserva_exames(request,pk):
    horario = get_object_or_404(Horario_Exames, pk=pk)
    if request.method == "POST":
        horario.livre = False
        horario.usuario = None
        horario.save()
        return HttpResponseRedirect(reverse('accounts:menu-reservas-exames-admin'))

    context = {'horario': horario}
    return render(request, 'accounts/delete-reserva-exames.html', context)



def reservas_espaco(request):
    clube = request.user.clube
    usuario = request.user
    horarios = Horario.objects.filter(livre=True,usuario=usuario.id)
    context = {'clube': clube,
                'horarios': horarios,}
    
    return render(request, 'accounts/list-reservas.html',context)



def reservas_exames(request):
    clube = request.user.clube
    usuario = request.user
    horarios = Horario_Exames.objects.filter(livre=True,usuario=usuario.id)
    context = {'clube': clube,
                'horarios': horarios,}
    
    return render(request, 'accounts/list-reservas-exames.html',context)



def delete_reserva_normal(request,pk):
    horario = get_object_or_404(Horario, pk=pk)
    if request.method == "POST":
        horario.livre = False
        horario.usuario = None
        horario.save()
        return HttpResponseRedirect(reverse('accounts:list-reservas'))

    context = {'horario': horario}
    return render(request, 'accounts/delete-reserva-normal.html', context)



def delete_reserva_exames_normal(request,pk):
    horario = get_object_or_404(Horario_Exames, pk=pk)
    if request.method == "POST":
        horario.livre = False
        horario.usuario = None
        horario.save()
        return HttpResponseRedirect(reverse('accounts:menu-reservas-exames'))

    context = {'horario': horario}
    return render(request, 'accounts/delete-reserva-exames-normal.html', context)



class UserUpdateView(generic.UpdateView):
    context_object_name = 'user'
    form_class = UserChangeForm
    model = Usuario
    template_class = UserChangeForm
    template_name = "accounts/update.html"

    def get_success_url(self):
        return reverse('accounts:list')



class UserDeleteView(generic.DeleteView):
    model = Usuario
    context_object_name = 'user'
    template_name = 'accounts/delete.html'

    def get_success_url(self):
        return reverse('accounts:list')