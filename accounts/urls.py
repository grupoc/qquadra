from django.urls import path

from . import views
app_name = 'accounts'
urlpatterns = [
    path('signup/', views.signup, name='signup'),
    path('list/', views.list_users, name='list'),
    path('update/<int:pk>/', views.UserUpdateView.as_view(), name='update'),
    path('delete/<int:pk>/', views.UserDeleteView.as_view(), name='delete'),
    path('menu-reservas/', views.menu_reservas_admin, name='menu-reservas'),
    path('list-reservas-admin/', views.reservas_espaco_admin, name='menu-reservas-admin'),
    path('reserva/delete/<int:pk>', views.delete_reserva, name='delete-reserva'),
    path('list-reservas-exames-admin/', views.reservas_exames_admin, name='menu-reservas-exames-admin'),
    path('reserva-exame/delete/<int:pk>', views.delete_reserva_exames, name='delete-reserva-exame'),
    path('list-reservas/', views.reservas_espaco, name='list-reservas'),
    path('list-reservas-exames/', views.reservas_exames, name='menu-reservas-exames'),
    path('reserva-normal/delete/<int:pk>', views.delete_reserva_normal, name='delete-reserva-normal'),
    path('reserva-exame-normal/delete/<int:pk>', views.delete_reserva_exames_normal, name='delete-reserva-exame-normal'),
]