from django.db import models
from django.contrib.auth.models import User,AbstractUser
from django.contrib.postgres.fields import ArrayField



class Modalidade(models.Model):    
    name = models.CharField(max_length=255)  
    icon_url = models.URLField(max_length=200, null=True, blank=True)

    def __str__(self):
        return f'{self.name}'



class Clube(models.Model):
    name = models.CharField(max_length=255)
    DESCR = models.TextField()
    adress = models.TextField()
    horario_inicial = models.TimeField()
    horario_final = models.TimeField()
    modalidades = models.ManyToManyField(Modalidade)

    def __str__(self):
        return f'{self.name}'



class Atletica(models.Model):    
    nome = models.CharField(max_length=255)
    instituicao = models.CharField(max_length=255)
    
    def __str__(self):
        return f'{self.nome}'



class Usuario(AbstractUser):    
    nome = models.CharField(max_length=255, blank=True, null=True)
    telefone = models.IntegerField( blank=True, null=True)
    foto = models.URLField(blank=True, null=True)
    email = models.CharField(max_length=255, blank=True, null=True)
    clube = models.ForeignKey(Clube, on_delete=models.CASCADE, blank=True, null=True)
    atletica = models.ForeignKey(Atletica, on_delete=models.CASCADE, blank=True, null=True)
    is_active = models.BooleanField(default=False)



class EspacoEsportivo(models.Model):    
    DESCR = models.TextField()
    modalidade = models.ForeignKey(Modalidade, on_delete=models.CASCADE)
    clube = models.ForeignKey(Clube, on_delete=models.CASCADE)  
    foto = models.URLField(blank=True, null=True)  

    def __str__(self):
        return (f'{self.DESCR} {self.clube}')



class TipoMaterial(models.Model):    
    DESCR = models.TextField()
    name = models.CharField(max_length=255)
    modalidade = models.ForeignKey(Modalidade, on_delete=models.CASCADE) 
    foto = models.URLField(blank=True, null=True)

    def __str__(self):
        return f'{self.name}'



class EstoqueMaterial(models.Model):    
    quantidade = models.IntegerField() 
    tipo_material = models.ForeignKey(TipoMaterial, on_delete=models.CASCADE)
    clube = models.ForeignKey(Clube, on_delete=models.CASCADE) 

    def __str__(self):
        return f'{self.tipo_material} {self.clube}'




class Dia(models.Model):
    espaco = models.ForeignKey(EspacoEsportivo, on_delete=models.CASCADE)
    dia = models.DateField()
    
    def __str__(self):
        return f'{self.dia} {self.espaco}'



class Horario(models.Model):
    dia = models.ForeignKey(Dia, on_delete=models.CASCADE)
    horario = models.TimeField()
    livre = models.BooleanField(default=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE,blank=True, null=True)
    
    def __str__(self):
        return f'{self.dia} {self.horario}'



class Dia_Exames(models.Model):
    clube = models.ForeignKey(Clube, on_delete=models.CASCADE)
    dia = models.DateField()
    
    def __str__(self):
        return f'{self.dia} {self.clube}'



class Horario_Exames(models.Model):
    dia = models.ForeignKey(Dia_Exames, on_delete=models.CASCADE)
    horario = models.TimeField()
    livre = models.BooleanField(default=False)
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE,blank=True, null=True)
    
    def __str__(self):
        return f'{self.dia} {self.horario}'