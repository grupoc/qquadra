# Generated by Django 3.2.7 on 2021-12-11 17:52

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0004_usuario_foto'),
    ]

    operations = [
        migrations.AlterField(
            model_name='usuario',
            name='foto',
            field=models.URLField(blank=True, default='https://www.ecp.org.br/wp-content/uploads/2017/12/default-avatar.png', null=True),
        ),
    ]
