from django.contrib import admin
from .models import Atletica,Clube,Usuario,Modalidade,EspacoEsportivo,TipoMaterial,EstoqueMaterial,Atletica, Dia, Horario,Dia_Exames, Horario_Exames
from django.contrib.auth import admin as auth_admin
from .forms import UserGroupForm,UserChangeForm

admin.site.register(Clube)
admin.site.register(Modalidade)
admin.site.register(EspacoEsportivo)
admin.site.register(TipoMaterial)
admin.site.register(EstoqueMaterial)
admin.site.register(Atletica)
admin.site.register(Dia)
admin.site.register(Horario)
admin.site.register(Dia_Exames)
admin.site.register(Horario_Exames)

@admin.register(Usuario)
class UserAdmin(auth_admin.UserAdmin):
    add_form = UserGroupForm
    form = UserChangeForm
    model = Usuario    
    fieldsets = auth_admin.UserAdmin.fieldsets + (
        ("Campos Personalizados", {"fields": ("nome","telefone","clube","atletica","foto",)}),
    )
    