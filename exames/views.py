from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.contrib.auth.decorators import login_required
from accounts.models import Usuario,Clube, EspacoEsportivo, Dia,Horario,Modalidade, Dia_Exames, Horario_Exames



@login_required(redirect_field_name='login')
def exames(request):    
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube        
        dias = Dia_Exames.objects.filter(clube=clube.id) 
        context = {'clube': clube,
                    'dias': dias}
    return render(request, 'exames/exames.html', context)



def detail_dia(request, pk):
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube
        dia = get_object_or_404(Dia_Exames,pk=pk)
        horarios = Horario_Exames.objects.filter(dia=dia.id)
        context = {'clube': clube,
                    'dia': dia,
                    'horarios': horarios,
                    }
    return render(request, 'exames/dia.html', context)



def create_reserva(request,pk):
    usuario = request.user
    horario = get_object_or_404(Horario_Exames, pk=pk)
    hora = Horario_Exames.objects.filter(usuario=usuario.id)
    quantidade = len(hora)
    if (str(usuario.groups.all()[0]) == "publico" and quantidade <= 0) or (str(usuario.groups.all()[0]) == "atletica" and quantidade <= 0) or (str(usuario.groups.all()[0]) == "admin"):
        if request.method == "POST":
            horario.livre = True
            horario.usuario = request.user
            horario.save()
            return HttpResponseRedirect(reverse('exames:detail-dia', args=(horario.dia.id, )))

        context = {'horario': horario}
        return render(request, 'exames/create-reserva.html', context)
    else:
        return HttpResponse("Você já fez reservas demais")