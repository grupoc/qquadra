from django.urls import path

from . import views

app_name = 'exames'
urlpatterns = [        
    path('', views.exames, name='index'),
    path('dias/<int:pk>', views.detail_dia, name='detail-dia'),
    path('exame/confirmar/<int:pk>', views.create_reserva, name='create-reserva'),
]