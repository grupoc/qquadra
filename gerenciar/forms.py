from django.forms import ModelForm
from accounts.models import EstoqueMaterial

class PostForm(ModelForm):
    class Meta:
        model = EstoqueMaterial
        fields = [
            'quantidade',
        ]
        labels = {
            'quantidade': 'Quantidade',
        }