from django.urls import path

from . import views

app_name = 'gerenciar'
urlpatterns = [        
    path('', views.gerenciar, name='index'),
    path('resetar/', views.resetar, name='resetar'),
    path('resetar/exames/', views.resetar_exames, name='resetar-exames'),
    path('atualizar/', views.atualizar, name='atualizar'),
    path('atualizar/exames/', views.atualizar_exames, name='atualizar-exames'),
    path('gerenciar/modalidades/', views.modalidades_list, name='lista-modalidades'),
    path('gerenciar/material/<int:pk>', views.material_list, name='lista-material'),
    path('gerenciar/material/estoque/<int:pk>', views.material_estoque, name='estoque-meterial'),
    path('gerenciar/material/estoque/update/<int:pk>', views.update_estoque, name='update-estoque'),
    
]