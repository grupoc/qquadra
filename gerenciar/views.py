from django.shortcuts import render, get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy
from django.views import generic
from django.contrib.auth.decorators import login_required
from accounts.models import Usuario,Clube, EspacoEsportivo, Dia,Horario,Modalidade,Dia_Exames,Horario_Exames,TipoMaterial,EstoqueMaterial
from datetime import timedelta
import datetime as dt
from django.utils import timezone
from accounts.decorators import allowed_users
from .forms import PostForm



def atualizar_dias(dias):
    today = timezone.now().date()
    espacos = EspacoEsportivo.objects.all()
    desatualizado=False
    for dia in Dia.objects.all():
        if today > dia.dia:
            Dia.objects.filter(id=dia.id).delete()
            desatualizado=True
    if desatualizado:
        for espaco in espacos:
            today = timezone.now().date()
            today = today + dt.timedelta(days=dias-1)
            dia = Dia.objects.create(espaco=espaco,dia=today) 
            start = espaco.clube.horario_inicial  
            while start != espaco.clube.horario_final:
                
                Horario.objects.create(dia=dia,horario=start)
                start = (dt.datetime.combine(dt.date(1,1,1), start) + dt.timedelta(hours=1)).time()
    return



def atualizar_dias_exames(dias):
    today = timezone.now().date()
    clubes = Clube.objects.all()
    desatualizado=False
    for dia in Dia_Exames.objects.all():
        if today > dia.dia:
            Dia_Exames.objects.filter(id=dia.id).delete()
            desatualizado=True
    if desatualizado:
        for clube in clubes:
            today = timezone.now().date()
            today = today + dt.timedelta(days=dias-1)
            dia = Dia_Exames.objects.create(clube=clube,dia=today)
            start = clube.horario_inicial  
            while start != clube.horario_final:
                
                Horario_Exames.objects.create(dia=dia,horario=start)
                start = (dt.datetime.combine(dt.date(1,1,1), start) + dt.timedelta(hours=1)).time()
    return



@login_required(redirect_field_name='login')
@allowed_users(allowed_roles=['admin'])
def gerenciar(request):
    
    return render(request, 'gerenciar/gerenciar.html')



@login_required(redirect_field_name='login')
def resetar(request):
    create_dias(63)
    return render(request, 'gerenciar/resetar.html')



@login_required(redirect_field_name='login')
def resetar_exames(request):
    create_dias_exames(63)
    return render(request, 'gerenciar/resetar.html')



@login_required(redirect_field_name='login')
def atualizar(request):

    atualizar_dias(63)
    return render(request, 'gerenciar/resetar.html')



@login_required(redirect_field_name='login')
def atualizar_exames(request):
    
    atualizar_dias_exames(63)
    return render(request, 'gerenciar/resetar.html')



def create_dias(dias):
    Dia.objects.all().delete()
    espacos = EspacoEsportivo.objects.all()
    for espaco in espacos:
        today = timezone.now().date()
        for i in range(dias):
            dia = Dia.objects.create(espaco=espaco,dia=today)  
            start = espaco.clube.horario_inicial  
            while start != espaco.clube.horario_final:
                
                Horario.objects.create(dia=dia,horario=start)
                start = (dt.datetime.combine(dt.date(1,1,1), start) + dt.timedelta(hours=1)).time()
                
            today = today + dt.timedelta(days=1)



def create_dias_exames(dias):
    Dia_Exames.objects.all().delete()
    clubes = Clube.objects.all()
    for clube in clubes:
        today = timezone.now().date()
        for i in range(dias):
            dia = Dia_Exames.objects.create(clube=clube,dia=today)  
            start = clube.horario_inicial  
            while start != clube.horario_final:
                
                Horario_Exames.objects.create(dia=dia,horario=start)
                start = (dt.datetime.combine(dt.date(1,1,1), start) + dt.timedelta(hours=1)).time()
                
            today = today + dt.timedelta(days=1)



def modalidades_list(request):
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube
        context = {'clube': clube,
                    }
    return render(request, 'gerenciar/lista-modalidades.html', context)



def material_list(request, pk):
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube
        modalidade = get_object_or_404(Modalidade,pk=pk)
        materials = TipoMaterial.objects.filter(modalidade=modalidade.id)
        context = {'clube': clube,
                    'modalidade':modalidade,
                    'materials':materials}
    return render(request, 'gerenciar/lista-materiais.html', context)



def material_estoque(request, pk):
    if request.user.is_authenticated:
        user = request.user
        clube = user.clube
        estoque = EstoqueMaterial.objects.filter(tipo_material=pk,clube=clube.id)
        context = {'clube': clube,
                    'estoque':estoque,}
    return render(request, 'gerenciar/estoque.html', context)



@login_required(redirect_field_name='login')
@allowed_users(allowed_roles=['admin'])
def update_estoque(request, pk):
    estoque = get_object_or_404(EstoqueMaterial, pk=pk)

    if request.method == "POST":
        form = PostForm(request.POST)
        if form.is_valid():
            estoque.quantidade = form.cleaned_data['quantidade']            
            estoque.save()
            return HttpResponseRedirect(
                reverse('gerenciar:estoque-meterial', args=(estoque.tipo_material.id, )))
    else:
        form = PostForm(
            initial={
                'quantidade': estoque.quantidade,
            })

    context = {'estoque': estoque, 'form': form}
    return render(request, 'gerenciar/update-estoque.html', context)